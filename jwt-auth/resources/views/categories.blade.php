@extends('header')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col"></div>
            <div class="col" style="display: flex;">
                @include('card')
            </div>
            <div class="col"></div>
        </div>
    </div>
@endsection
